const dataArray = require("./data.js");
let splittedIPs = dataArray.map((person) => {
  let ipAddress = person.ip_address;
  return ipAddress.split(".");
});
const sumOfFourthIPs = splittedIPs.reduce((sumOfFourthTerm, everyIp) => {
  let IP = Number(everyIp[3]);
  return sumOfFourthTerm + IP;
}, 0);

console.log(sumOfFourthIPs);
