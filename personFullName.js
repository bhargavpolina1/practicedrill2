let dataArray = require("./data.js");

let personFullname = dataArray.map((person) => {
  let fullName = person.first_name + " " + person.last_name;
  person["full_name"] = fullName;

  return person;
});

console.log(personFullname);
