const dataArray = require("./data.js");
let splittedIPs = dataArray.map((person) => {
  let ipAddress = person.ip_address;
  return ipAddress.split(".");
});
const sumOfSecondIPs = splittedIPs.reduce((sumOfSecondTerm, everyIp) => {
  let IP = Number(everyIp[1]);
  return sumOfSecondTerm + IP;
}, 0);

console.log(sumOfSecondIPs);
