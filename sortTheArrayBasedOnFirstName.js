const dataArray = require("./data.js");

const sortByFirstName = dataArray.sort((initialPerson, currentPerson) => {
  let conInitialPerson = initialPerson.first_name.toLowerCase();
  let conCurrentPerson = currentPerson.first_name.toLowerCase();

  if (conInitialPerson > conCurrentPerson) {
    return -1;
  } else if (conInitialPerson < conCurrentPerson) {
    return 1;
  } else {
    return 0;
  }
});

console.log(sortByFirstName);
