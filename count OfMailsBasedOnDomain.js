const dataArray = require("./data.js");

const countOfMails = dataArray.reduce((countOfMails, person) => {
  let mail = person.email;
  let isEndsWithOrg = mail.endsWith(".org");
  let isEndsWithAu = mail.endsWith(".au");
  let isEndsWithCom = mail.endsWith(".com");

  if (isEndsWithOrg) {
    if (countOfMails.hasOwnProperty("Org")) {
      countOfMails["Org"] += 1;
    } else {
      countOfMails["Org"] = 1;
    }
  } else if (isEndsWithAu) {
    if (countOfMails.hasOwnProperty("Au")) {
      countOfMails["Au"] += 1;
    } else {
      countOfMails["Au"] = 1;
    }
  } else if (isEndsWithCom) {
    if (countOfMails.hasOwnProperty("Com")) {
      countOfMails["Com"] += 1;
    } else {
      countOfMails["Com"] = 1;
    }
  }
  return countOfMails;
}, {});

console.log(countOfMails);
